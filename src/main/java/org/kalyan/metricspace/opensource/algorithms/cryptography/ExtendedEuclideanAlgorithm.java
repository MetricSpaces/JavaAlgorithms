package org.kalyan.metricspace.opensource.algorithms.cryptography;

import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;


/**
 * 
 * @author Kalyan
 *
 */
public class ExtendedEuclideanAlgorithm {
	
	/**
	 * This method computes the inverse of a mod b where a and b are two positive integers
	 * within the range of Java int data type
	 * using the Extended Euclidean algorithm.
	 * The method throws a RuntimeException if at least one of the inputs
	 * is a non-positive integer.
	 * The method also throws a RuntimeException if a and b are not coprimes.
	 * The correctness for the large integers is yet to be tested.
	 * @param a
	 * @param b
	 * @param c
	 * @param e
	 * @return
	 * @throws InvalidInputException
	 */
	public int moduloInverseRecurse(int a, int b, int c, int e) 
			throws InvalidInputException{
		int q = a/b;
		int r = a%b;
		if(r==0){
			if(b==1){
				return c;
			} else {
				throw new InvalidInputException("The number and modulus is not coprime,"
						+ "inverse doesn't exist. ");
			}
		}
		int d = -q*c+e;
		return moduloInverseRecurse(b,r,d,c);
	}
	
	public int moduloInverse(int number, int modulus)
			throws InvalidInputException{
		if(number<=0 || modulus<=0){
			throw new InvalidInputException("Enter only positive integers");
		}
		int inverse = moduloInverseRecurse(modulus, number,1,0);
		inverse+= (inverse<0)? modulus:0;
		System.out.println("{"+number+","+modulus+ ","+inverse+ "},");
		return inverse;
	}
}
