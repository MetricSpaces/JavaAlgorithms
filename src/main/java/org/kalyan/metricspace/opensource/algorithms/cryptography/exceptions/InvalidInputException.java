package org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions;

/**
 * 
 * @author Kalyan
 *
 */

public class InvalidInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1001L;
	
	public InvalidInputException(String message){
		super(message);
	}
}
