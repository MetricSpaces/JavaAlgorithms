package org.kalyan.metricspace.opensource.algorithms.cryptography;

/**
 * 
 * @author Kalyan
 *
 */
public class LCGPseudoRandomNumberGenerator {
	
	private static final int SEED = 12345;
	private static final int MULTIPLIER = 1103515245;
	private static final int ADDER = 12345;
	private static final int MODULUS = 2^31;
	private static int state = SEED;
	
	private final int next(){
		state = (MULTIPLIER*state + ADDER)%MODULUS;
		return state;
	}
	

	public static void main(String[] args) {
		LCGPseudoRandomNumberGenerator s = new LCGPseudoRandomNumberGenerator();
		int first = s.next();
		int next = first;
		int count = 0;
		do {
			count++;
			next = s.next();
			System.out.println(next);
		} while(next != first);
		System.out.println("Count:"+count);

	}

}
