package org.kalyan.metricspace.opensource.algorithms.cryptography;

import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;


/**
 * 
 * @author Kalyan
 *
 */
public class EuclideanAlgorithm {
	
	/**
	 * This method computes the GCD or HCF of two positive integers
	 * within the range of Java int data type
	 * using the Euclidean algorithm.
	 * The method throws a RuntimeException if at least one of the inputs
	 * is a non-positive integer.
	 * The correctness for the large integers is yet to be tested.
	 * @param a
	 * @param b
	 * @return
	 * @throws InvalidInputException
	 */
	private int gcdRecurse(int a,int b)
			throws InvalidInputException{
		if(a<=0 || b<=0){
			throw new InvalidInputException("Enter only positive integers");
		}
		int remainder = a%b;
		if(remainder==0){
			//System.out.println(b+"},");
			return b;
		} else {
			return gcdRecurse(b,remainder);
		}
	}
	
	public int gcd(int a, int b)
			throws InvalidInputException{
		//System.out.print("{"+a+","+b+",");
		return gcdRecurse(a,b);
	}

}
