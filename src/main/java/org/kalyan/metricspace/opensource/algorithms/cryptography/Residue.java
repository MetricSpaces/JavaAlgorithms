package org.kalyan.metricspace.opensource.algorithms.cryptography;

import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;


/**
 * 
 * @author Kalyan
 *
 */
public class Residue {
	
	/**
	 * If a,m are two positive integers, then the residue r is a mod m i.e. the remainder
	 * when a is divided by m, where m is called the modulus.
	 * 
	 * @param number
	 * @param modulus
	 * @return
	 * @throws InvalidInputException
	 */
	public static int residue(int a, int m)
			throws InvalidInputException{
		if(a<1 || m<1){
			throw new InvalidInputException("Number and Modulus "
					+ "should be positive integers");
		}
		return a%m;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
