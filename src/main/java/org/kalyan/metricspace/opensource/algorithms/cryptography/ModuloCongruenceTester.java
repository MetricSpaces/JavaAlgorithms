package org.kalyan.metricspace.opensource.algorithms.cryptography;

import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;


/**
 * 
 * @author Kalyan
 *
 */
public class ModuloCongruenceTester {
	
	/**
	 * a mod m = b mon m read as a modulo m is congruent to b modulo m iff
	 * m|a-b read as m divides a-b
	 * @param a
	 * @param m
	 * @param b
	 * @return
	 * @throws InvalidInputException
	 */
	public boolean isCongruent(int a, int m, int b)
		throws InvalidInputException{
		if(m<1){
			throw new InvalidInputException("Modulus should be positive");
		}
		if(a%m == b%m){
			System.out.println(a+" mod "+m+" = "+b+" mod "+m);
			return true;
		}
		else {
			System.out.println(a+" mod "+m+" != "+b+" mod "+m);
			return false;
		}
	}
}
