package org.kalyan.metricspace.opensource.algorithms.cryptography;

import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;


/**
 * 
 * @author Kalyan
 *
 */
public class LinearCongruences {
	
	/**
	 * Linear congruence are in the form ax = b mod m,where we have to find a
	 * solution for x if it exists or else we throw an Exception
	 * @param a
	 * @param b
	 * @param c
	 * @param e
	 * @return
	 * @throws InvalidInputException
	 */
	private int moduloInverseRecurse(int a, int b, int c, int e) 
			throws InvalidInputException{
		int q = a/b;
		int r = a%b;
		if(r==0){
			if(b==1){
				return c;
			} else {
				throw new InvalidInputException("Invalid linear congruence");
			}
		}
		int d = -q*c+e;
		return moduloInverseRecurse(b,r,d,c);
	}
	
	public int solveLinearCongruence(int a,int b, int m)
	 		throws InvalidInputException{
		if(a<=0 || b<=0 || m<=0){
			throw new InvalidInputException("Enter only positive integers");
		}
		int inverse = moduloInverseRecurse(m,a,1,0);
		inverse+= (inverse<0)? m:0;
		int solution = inverse*b;
		solution = solution>m? solution%m:solution;
		System.out.println("The solution to the linear congruence "+a+"x="
						+b+" mod "+m+" is "+solution+" mod "+m);
		return solution;
	}
}
