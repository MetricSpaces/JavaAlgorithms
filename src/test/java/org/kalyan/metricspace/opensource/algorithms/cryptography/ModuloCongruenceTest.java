package org.kalyan.metricspace.opensource.algorithms.cryptography;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;

@RunWith(Parameterized.class)
public class ModuloCongruenceTest {
	int a;
	int b;
	int c;
	ModuloCongruenceTester mc;
	@Rule
	public ErrorCollector ec = new ErrorCollector();
	
	public ModuloCongruenceTest(int a, int b, int c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	@Before
	public void setup(){
		mc = new ModuloCongruenceTester();
	}
	
	@Parameters
	public static Collection input(){
		return Arrays.asList(new Object[][]{ 
			{414,413,1},
			{463,413,50}});
	}
	
	@Test
	public void addTest() {
		try {
			assertTrue(mc.isCongruent(a, b, c));
		}
		catch(InvalidInputException e){
			ec.addError(e);
		}
	}
	
	@After
	public void teardown(){
		mc = null;
	}

}
