package org.kalyan.metricspace.opensource.algorithms.cryptography;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;

@RunWith(Parameterized.class)
public class ExtendedEuclideanAlgorithmTest {
	int a;
	int b;
	int expected;
	ExtendedEuclideanAlgorithm ea;
	@Rule
	public ErrorCollector ec = new ErrorCollector();
	
	public ExtendedEuclideanAlgorithmTest(int a, int b, int expected){
		this.a = a;
		this.b = b;
		this.expected = expected;
	}
	
	@Before
	public void setup(){
		ea = new ExtendedEuclideanAlgorithm();
	}
	
	@Parameters
	public static Collection input(){
		return Arrays.asList(new Object[][]{ 
			{67,12,7},
			{12,67,28},
			{19,141,52},
			{55,89,34},
			{4,9,7},
			{56,93,5},
			{19,141,52}});
	}
	
	@Test
	public void addTest() {
		try {
			assertEquals(expected, ea.moduloInverse(a, b));
		}
		catch(InvalidInputException e){
			ec.addError(e);
		}
	}
	
	@After
	public void teardown(){
		ea = null;
	}

}
