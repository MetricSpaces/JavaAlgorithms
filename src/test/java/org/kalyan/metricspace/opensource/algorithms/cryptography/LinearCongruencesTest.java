package org.kalyan.metricspace.opensource.algorithms.cryptography;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;

@RunWith(Parameterized.class)
public class LinearCongruencesTest {
	int a;
	int b;
	int c;
	int expected;
	LinearCongruences lc;
	@Rule
	public ErrorCollector ec = new ErrorCollector();
	
	public LinearCongruencesTest(int a, int b, int c,int expected){
		this.a = a;
		this.b = b;
		this.c = c;
		this.expected = expected;
	}
	
	@Before
	public void setup(){
		lc = new LinearCongruences();
	}
	
	@Parameters
	public static Collection input(){
		return Arrays.asList(new Object[][]{ 
			{19,4,141,67},
			{7,3,10,9}});
	}
	
	@Test
	public void addTest() {
		try {
			assertEquals(expected,lc.solveLinearCongruence(a, b, c));
		}
		catch(InvalidInputException e){
			ec.addError(e);
		}
	}
	
	@After
	public void teardown(){
		lc = null;
	}

}
