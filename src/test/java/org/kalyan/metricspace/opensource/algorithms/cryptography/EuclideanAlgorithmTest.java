package org.kalyan.metricspace.opensource.algorithms.cryptography;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.kalyan.metricspace.opensource.algorithms.cryptography.exceptions.InvalidInputException;

@RunWith(Parameterized.class)
public class EuclideanAlgorithmTest {
	int a;
	int b;
	int expected;
	EuclideanAlgorithm ea;
	@Rule
	public ErrorCollector ec = new ErrorCollector();
	
	public EuclideanAlgorithmTest(int a, int b, int expected){
		this.a = a;
		this.b = b;
		this.expected = expected;
	}
	
	@Before
	public void setup(){
		ea = new EuclideanAlgorithm();
	}
	
	@Parameters
	public static Collection input(){
		return Arrays.asList(new Object[][]{ 
			{10,11,1},
			{11,10,1},
			{20,10,10},
			{10,20,10},
			{315,81,9},
			{81,315,9},
			{45,5,5},
			{5,45,5},
			{84,30,6},
			{30,84,6},
			{27,21,3},
			{21,27,3},
			{973,301,7},
			{301,973,7},
			{67,12,1},
			{12,67,1},
			{12,18,6},
			{18,12,6}});
	}
	
	@Test
	public void addTest() {
		try {
			assertEquals(expected, ea.gcd(a, b));
		}
		catch(InvalidInputException e){
			ec.addError(e);
		}
	}
	
	@After
	public void teardown(){
		ea = null;
	}

}
